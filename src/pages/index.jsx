import { getCategoryNames, getCategoryPhotos } from "../lib/photos";
import { getLayout } from "../components/Layout";
import Gallery from "../components/Gallery";

export default function Index(props) {
  return <Gallery photos={props.photos} />;
}

Index.getLayout = getLayout;

export async function getStaticProps({ params }) {
  const photos = getCategoryPhotos(["portraits", "in-the-city"]);
  const categories = getCategoryNames();

  return {
    props: {
      photos,
      categories,
    },
  };
}
