import { getCategoryNames, getCategoryPhotos } from "../lib/photos";
import { getLayout } from "../components/Layout";
import Gallery from "../components/Gallery";

export default function Category(props) {
  return <Gallery parent={props.parent} child={props.child} photos={props.photos} />;
}

Category.getLayout = getLayout;

export async function getStaticPaths() {
  function getCategoryPaths() {
    const ret = [];

    for (let parent of getCategoryNames()) {
      for (let child of parent.children) {
        ret.push({
          params: {
            category: [parent.slug, child.slug],
          },
        });
      }
    }

    return ret;
  }

  return {
    paths: getCategoryPaths(),
    fallback: false,
  };
}

export async function getStaticProps({ params }) {
  let [parentSlug, childSlug] = params.category;

  const photos = getCategoryPhotos([parentSlug, childSlug]);
  const categories = getCategoryNames();

  let parent = categories.find((category) => category.slug === parentSlug);
  let child = parent.children.find((child) => child.slug === childSlug);

  return {
    props: {
      parent,
      child,
      photos,
      categories,
    },
  };
}
