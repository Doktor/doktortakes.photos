import styles from "./SidebarList.module.scss";

export default function SidebarList(props) {
  function createListItem(item, index) {
    return (
      <li className={styles.SidebarListItem} key={index}>
        {item}
      </li>
    );
  }

  return (
    <ul className={[props.className, styles.SidebarList].join(" ")}>
      {props.children.map((item, index) => createListItem(item, index))}
    </ul>
  );
}
