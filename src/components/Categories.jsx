import styles from "./Categories.module.scss";
import Category from "./Category";
import SidebarList from "./SidebarList";

export default function Categories(props) {
  return (
    <ul className={styles.Categories}>
      {props.categories?.map((category) => (
        <li key={category.name}>
          <h3 className={styles.SubcategoryName}>{category.name}</h3>

          <SidebarList className={styles.Subcategories}>
            {category.children?.map((subcategory) => (
              <Category
                key={`${category.slug}/${subcategory.slug}`}
                category={category}
                subcategory={subcategory}
                onClick={props.onClick}
              />
            ))}
          </SidebarList>
        </li>
      ))}
    </ul>
  );
}
