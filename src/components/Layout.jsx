import styles from "./Layout.module.scss";
import Sidebar from "./Sidebar";

export default function Layout(props) {
  return (
    <main className={styles.Main}>
      <Sidebar categories={props.categories} />
      <div className={styles.Content}>{props.children}</div>
    </main>
  );
}

export const getLayout = (page, { categories }) => {
  return <Layout categories={categories}>{page}</Layout>;
};
