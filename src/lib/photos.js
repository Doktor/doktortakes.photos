import fs from "fs";
import path from "path";

const BASE_PATH = process.cwd();
const PUBLIC_PATH = path.join(BASE_PATH, "public");
const CATEGORIES_PATH = path.join(PUBLIC_PATH, "images");

export function getCategoryNames() {
  const categories = fs.readdirSync(CATEGORIES_PATH, { withFileTypes: true });

  const ret = [];

  for (let category of categories) {
    let parent = {
      slug: category.name,
      name: category.name.replaceAll("-", " "),
      children: [],
    };

    for (let subcategory of fs.readdirSync(path.join(CATEGORIES_PATH, category.name), { withFileTypes: true })) {
      let child = {
        slug: subcategory.name,
        name: subcategory.name.replaceAll("-", " "),
      };

      parent.children.push(child);
    }

    ret.push(parent);
  }

  return ret;
}

export function getCategoryPhotos(category) {
  const categoryPath = path.join(CATEGORIES_PATH, ...category);

  const fileNames = fs.readdirSync(categoryPath);
  fileNames.sort((a, b) => b.localeCompare(a));

  return fileNames.map((filename) => "/" + path.relative(PUBLIC_PATH, path.join(categoryPath, filename)));
}
