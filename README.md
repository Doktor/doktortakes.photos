# About

This is the repository for my photography portfolio website, [doktortakes.photos](https://doktortakes.photos).

It's built using Next.js and React and intended to be deployed as a static site.

# Instructions

## Start the development server

`npm run dev`

The server will be accessible at `http://localhost:3001`.

## Copy image files

Copy image files into the `./public/images/` directory. The expected directory structure is:

```
public/images/
│
├── category1/
│   ├── subcategory1/
│   │   └── (image files)
│   └── subcategory2/
│       └── (image files)
│
├── category2/
│   ├── subcategory3/
│   │   └── (image files)
│   └── subcategory4/
│       └── (image files)
│
└── ...
```

## Generate image thumbnails

```bash
cd ./public/
./generate-thumbnails.sh
```

## Build for production

`npm run build`

The build artifacts will be located in `/out/` and can be served directly using `nginx` or a similar web server.
